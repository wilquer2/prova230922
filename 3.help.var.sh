#!/bin/bash

echo "Variaveis"

echo " variavel pwd: Mostra o diretorio de trabalho autal"
echo $pwd

echo "Variavel home: Mostra o diretorio base do usuário atual"
echo $home

echo "Variavel term: Especifica o de terminal a ser emulado ao executar o shell"
echo $term

echo "Variavel path: Mostra uma lista de diretórios que o sistema ira verificar ao procurar por comandos"
echo $path

echo "Variavel hostname: Mostra o nome do host do computador neste momento"
echo $hostname

echo "Variavel user: Mostra o usuário que esta conectado atualmente"
echo $user

echo "Variavel Shell: Exibe o shell utilizado"
echo $shell

echo "Variavel uid: Exibe o user ID de quem executou o script"
echo $uid

echo "Variavel Lang: Exibe o idioma que o sistema esta usando"
echo $lang

echo "Variavel OStype: Exibe o tipo de sistema operacional"
echo $OStype

echo "Variavel random: Gera um numero randômico e exibe na tela"
echo $random

echo "Variavel $*: exibe todos os parâmetros informados"
echo $*






